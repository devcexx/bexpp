/*
 This file is part of Bexpp.

 Bexpp is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Bexpp is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Bexpp.  If not, see <http://www.gnu.org/licenses/>.
 */

//Archivo de código escrito por Roberto Guillén, a 17/10/2016

#include <stdio.h>
#include "expr.h"

int powe(int base, int exponent){
    if (exponent == 0) return 1;
    int p = 1;
    for (int i = 0; i < exponent; i++){
        p *= base;
    }
    return p;
}

void print_help(){
    printf("Usage: truth-table -v <variables> <expr>\n"
                   "Example: truth-table -v abc \"ab*(b+c)\"\n");
    exit(1);
}

int main(int argc, const char* argv[]) {
    if (argc != 4){
        print_help();
    }

    if (strcmp(argv[1], "-v") != 0){
        print_help();
    }

    const char* varstr = argv[2];
    const char* expr = argv[3];

    int vcount = (int)strlen(varstr);
    variable* variables[vcount];
    for (int i = 0; i < vcount; i++){
        char c = varstr[i];
        if (search_var(variables, i, c) != NULL){
            printf("Duplicated variable: %c\n", c);
            exit(2);
        } else {
            variable* v = malloc(sizeof(variable));
            v->def = c;
            variables[i] = v;
        }
    }
    expr_parse_err err;
    operation* op = parse_expr((char*)expr, 0, (int)strlen(expr), variables, vcount, &err);
    if (op == NULL){
        printf("Parsing error at offset %d! ", err.offset);
        switch (err.errno){
            case PERR_UNKNOWN_SYMBOL:
                printf("Unknown symbol.\n");
                break;
            case PERR_MALFORMED_EXPR:
                printf("Malformed boolean expression.\n");
                break;
            case PERR_UNBALANCED_BRAKETS:
                printf("Unbalanced brackets.\n");
                break;
        }
        exit(3);
    }else {
        for (int i = 0; i < vcount; i++){
            printf("%c ", variables[i]->def);
        }
        printf("S\n");
        int count = powe(2, vcount);
        for(long i = 0; i < count; i++){
            for (int j = 0; j < vcount; j++){
                variables[j]->value = (i >> (vcount - j - 1) & 0x1) > 0;
                printf("%d ", variables[j]->value);
            }
            printf("%d\n", evaluate(op));
        }

    }
    return 0;
}