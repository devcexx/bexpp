# Bexpp

Bexpp es una pequeña libería libre de dependencias escrita en C que facilita
el análisis de funciones booleanas y la evaluación de las mismas.
Esta librería solo se compone del archivo ```expr.h```, ya que el archivo
```main.c``` presente en este repositorio, tan solo es un pequeño programa
de ejemplo para probar el funcionamiento de la librería a través de la
composición de tablas de verdad.

Los operadores que están permitidos desde la versión ```1.0``` se describen
a continuación. Además, están ordenados en orden de precedencia, siendo el
primero el de mayor precedencia.

  - ```'```: Operador de negación (NOT). Se escribe después de la variable
    que se desea negar. Ejemplo: en la expresión ```db+a'c```,
    la ```a``` seríá la única variable negada.

  - ```^```: Operador OR Exclusivo (XOr). Ejemplo: la función
    ```a^b``` en una simbología más común equivaldría a ```a ⊕ b```

  - ```*```: Operador AND. Por comodidad, éste operador puede ser omitido
    si sus operandos son otras dos variables (```a*b``` sería
    equivalente a ```ab```, pero ```a(b+a)``` no sería una
    fórmula válida.

  - ```+```: Operador OR.

Además de estos operadores se pueden utilizar las constantes ```0``` y ```1```
como los valores ```false``` y ```true```, respectivamente.

Ejemplos:

  - ```a + b + c```
  - ```a * (b + cde')```
  - ```a + (b' + c^d)```
  - ```a + 1```
  - ```a0 + ab```
  - ```a1b + a0c```

Para utilizar Bexpp en su proyecto, simplemente descargue el archivo de cabeceras
```expr.h``` y guárdelo en su proyecto. Entre los métodos, las estructuras y
las macros que define la cabecera, solo necesitará algunos de ellos:

  - Método: ```operation* parse_expr(char*, int, int, variable**, int, expr_parse_err*)```.
    Analiza una función booleana y retorna un puntero a una operación equivalente.
  
    - Parámetro 0: un puntero que corresponda a la ubicación en memoria de la expresión
      a procesar.
    - Parámetro 1: el desplazamiento (offset) desde el inicio de la cadena anterior
      a partir del cual se deberá comenzar a leer. El valor ```0``` indicará que se
      empiece a leer desde el principio.
    - Parámetro 2: el número de carácteres a leer.
    - Parámetro 3: Un array de punteros de ```variable``` que corresponden a las
      variables que se deben esperar que estén presentes en la expresión.
    - Parámetro 4: El número de variables del array del parámetro anterior.
    - Parámetro 5: Un puntero a un objeto que contendrá información de un
      posible error.

  En el caso de que se produzca un error de parseo, la función retornará ```NULL``` y
  se pasará información relativa del error a través del puntero pasado por el último
  parámetro.
  
  - Tipo ```expr_parse_err```: Contiene los campos ```errno``` y ```offset``` que indican
    el id de error y la ubicación en la expresión donde se produjo el error, respectivamente.
    Los posibles valores de ```errno``` se corresponden con las macros ```PERR_UNKNOWN_SYMBOL```, 
    ```PERR_UNBALANCED_BRAKETS``` y ```PERR_MALFORMED_EXPR```.

  - Tipo ```variable```: Contiene información acerca de una variable. Contiene los campos ```def``` y
    ```value```, que determinan el nombre de la variable y su valor respectivamente. Cabe destacar, que
    el campo ```value``` solo se tiene en cuenta a la hora de evaluar una expresión booleana, no a la
    hora de analizarla.

  - Método: ```bool evaluate(operation*)```. Evalúa la operación indicada, utilizando el valor actual
    de las variables y devuelve el valor final como un valor booleano.

Para ver un ejemplo sobre cómo utilizar estos métodos, véase el archivo ```main.c``` de este repositorio.
  
