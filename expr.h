/*
 This file is part of Bexpp.

 Bexpp is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Bexpp is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Bexpp.  If not, see <http://www.gnu.org/licenses/>.
 */

//Archivo de código escrito por Roberto Guillén, a 17/10/2016

#ifndef boole_expr_h
#define boole_expr_h

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define OPERAND_VAR 0x1
#define OPERAND_NESTED_OPERATION 0x2
#define OPERAND_CONSTANT 0x3

#define CONSTANT_FALSE (void*)0
#define CONSTANT_TRUE (void*)1

#define OP_NONE 0x0
#define OP_NEGATE 0x1
#define OP_XOR 0x2
#define OP_AND 0x3
#define OP_OR 0x4

#define PERR_UNKNOWN_SYMBOL 0x1
#define PERR_UNBALANCED_BRAKETS 0x2
#define PERR_MALFORMED_EXPR 0x3

typedef struct {
    char def;
    bool value;
} variable;

typedef struct {
    uint8_t type;
    void* ptr;
} operand;

typedef struct {
    uint8_t operator;
    operand** operands;
} operation;


typedef struct {
    uint8_t errno;
    int offset;
} expr_parse_err;

extern inline int operator_count(int op){
    switch (op){
        case OP_NONE:
        case OP_NEGATE:
            return 1;
        case OP_AND:
        case OP_OR:
        case OP_XOR:
            return 2;
        default:
            return -1;
    }
}

extern inline bool evaluate(operation* operation){
    int op_count = operator_count(operation->operator);
    bool values[op_count];
    for (int i = 0; i < op_count; i++){
        operand* oper = operation->operands[i];
        switch (oper->type){
            case OPERAND_CONSTANT:
                values[i] = oper->ptr == CONSTANT_TRUE ? true : false;
                break;
            case OPERAND_VAR:
                values[i] = ((variable*)oper->ptr)->value;
                break;
            case OPERAND_NESTED_OPERATION:
                values[i] = evaluate(oper->ptr);
                break;
        }
    }

    switch(operation->operator){
        case OP_NONE:
            return values[0];
        case OP_NEGATE:
            return !values[0];
        case OP_AND:
            return values[0] && values[1];
        case OP_OR:
            return values[0] || values[1];
        case OP_XOR:
            return values[0] ^ values[1];
    }
}

extern variable* search_var(variable** vars, int len, char match){
    for (int i = 0; i < len; i++){
        if (vars[i]->def == match) return vars[i];
    }
    return NULL;
}

extern inline bool is_valid_var_name(char ch){
    return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
}

extern inline expr_parse_err create_parse_error(uint8_t errno, int offset){
    expr_parse_err err = {errno, offset};
    return err;
}

extern inline operation* parse_expr(char* buf, int off, int len,
                                    variable** variables, int vlen,
                                    expr_parse_err* error) {
    if (buf[off] == '(' && buf[off + len - 1] == ')'){
        off++;
        len-=2;
    }

    operation* out = malloc(sizeof(operation));
    int found_op_type = 0;
    int found_op_offset = -1;
    int strlen = off + len;


    //Carácter actual
    char c;

    //true si el último carácter (sin tener en cuenta las negaciones)
    // se correspondía a una variable
    bool last_was_var = false;

    //El nivel de paréntesis. 0 se refiere a que la búsqueda se encuentra
    //en el nivel superior. 1, que se encuentra bajo la acción de 1 paréntesis,
    //etc. Si este valor es != 0 al final del bucle, los paréntesis no están
    //bien balanceados.
    int level = 0;
    for (int i = off; i < strlen; i++){
        c = buf[i];
        if (c == '('){
            level++;
        } else if (c == ')'){
            if (level == 0){
                *error = create_parse_error(PERR_UNBALANCED_BRAKETS, i);
                return NULL;
            } else level--;
        } else if (c == '+') {

            //Las operaciones solo se buscan fuera de los paréntesis, claro
            if (level == 0) {
                found_op_type = OP_OR;
                found_op_offset = i;
            }
        } else if (c == '*'){

            //Solo si estamos fuera de los paréntesis y no se ha encontrado
            //ninguna operación aún o las que se han encontrado tienen
            //una prioridad mayor que la operacion AND.
            if (level == 0 && (found_op_type < OP_AND)){
                found_op_type = OP_AND;
                found_op_offset = i;
            }
        } else if (c == '^') {
            if (level == 0 && (found_op_type < OP_XOR)){
                found_op_type = OP_XOR;
                found_op_offset = i;
            }
        } else if (c == '\'') {
            if (level == 0 && (found_op_type < OP_NEGATE)){
                found_op_type = OP_NEGATE;
                found_op_offset = i;
            }

            //Evitar que last_was_var = false;
            continue;
        } else if (is_valid_var_name(c) || c == '0' || c == '1') {
            if (c != '0' && c != '1'){
                //Variable
                variable* var = search_var(variables, vlen, c);
                if (var == NULL){
                    *error = create_parse_error(PERR_UNKNOWN_SYMBOL, i);
                    return NULL;
                }
            }

            if (last_was_var) {
                //Esto sucede cuando el programa encuentra algo tal que
                //  a'b, que realmente es a' * b, pero con la multiplicación
                //omitida.
                if (level == 0 && (found_op_type < OP_AND)){
                    found_op_type = OP_AND;
                    found_op_offset = i;
                }
            } else {
                last_was_var = true;
            }

            //Evitar que last_was_var = false;
            continue;
        } else if (c != ' '){
            *error = create_parse_error(PERR_UNKNOWN_SYMBOL, i);
            return NULL;
        }
        last_was_var = false;
    }

    if (level > 0){
        *error = create_parse_error(PERR_UNBALANCED_BRAKETS, strlen);
        return NULL;
    } else {
        out->operator = found_op_type;
        if (found_op_type == OP_NEGATE || found_op_type == OP_NONE) {
            operand *op = malloc(sizeof(operand));
            operand **operands = malloc(sizeof(operand *));
            operands[0] = op;
            out->operands = operands;
            int olen;
            if (found_op_type == OP_NONE){
                olen = len;
            } else {
                olen = found_op_offset - off;
            }
            if (olen < 1){
                *error = create_parse_error(PERR_MALFORMED_EXPR, strlen);
                return NULL;
            } else if (olen == 1){
                if (buf[off] == '0'){
                    op->type = OPERAND_CONSTANT;
                    op->ptr = CONSTANT_FALSE;
                } else if (buf[off] == '1') {
                    op->type = OPERAND_CONSTANT;
                    op->ptr = CONSTANT_TRUE;
                } else {
                    op->type = OPERAND_VAR;
                    op->ptr = search_var(variables, vlen, buf[off]);
                }
            } else {
                op->type = OPERAND_NESTED_OPERATION;
                op->ptr = parse_expr(buf, off, olen, variables, vlen,
                                     error);
                if (op->ptr == NULL) return NULL;
            }
        } else {
            operand *op1 = malloc(sizeof(operand));
            operand *op2 = malloc(sizeof(operand));
            operand **operands = malloc(sizeof(operand *) * 2);
            operands[0] = op1;
            operands[1] = op2;
            out->operands = operands;

            int len1 = found_op_offset - off;
            int len2;
            int off2;

            if (found_op_type == OP_AND && buf[found_op_offset] != '*') {
                //Operador omitido
                off2 = found_op_offset;
            } else {
                //Operador explícito
                off2 = found_op_offset + 1;
            }
            len2 = strlen - off2;

            if (len1 < 1 || len2 < 1) {
                *error = create_parse_error(PERR_MALFORMED_EXPR, strlen);
                return NULL;
            } else {
                if (len1 == 1) {
                    if (buf[off] == '0'){
                        op1->type = OPERAND_CONSTANT;
                        op1->ptr = CONSTANT_FALSE;
                    } else if (buf[off] == '1') {
                        op1->type = OPERAND_CONSTANT;
                        op1->ptr = CONSTANT_TRUE;
                    } else {
                        op1->type = OPERAND_VAR;
                        op1->ptr = search_var(variables, vlen, buf[off]);
                    }
                } else {
                    op1->type = OPERAND_NESTED_OPERATION;
                    op1->ptr = parse_expr(buf, off, len1, variables, vlen,
                                          error);
                    if (op1->ptr == NULL) return NULL;
                }

                if (len2 == 1) {
                    if (buf[off2] == '0'){
                        op2->type = OPERAND_CONSTANT;
                        op2->ptr = CONSTANT_FALSE;
                    } else if (buf[off2] == '1') {
                        op2->type = OPERAND_CONSTANT;
                        op2->ptr = CONSTANT_TRUE;
                    } else {
                        op2->type = OPERAND_VAR;
                        op2->ptr = search_var(variables, vlen, buf[off2]);
                    }
                } else {
                    op2->type = OPERAND_NESTED_OPERATION;
                    op2->ptr = parse_expr(buf, off2, len2, variables, vlen,
                                          error);
                    if (op2->ptr == NULL) return NULL;
                }
            }
        }
    }
    return out;
}



#endif //BOOLEALGEBRA_EXPR_H
